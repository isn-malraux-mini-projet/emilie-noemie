from PIL import Image
nom_image= input("Quel est le nom de l'image? ")
image = Image.open(nom_image)
image.show()



def negatifcouleur():
    """affiche l'image en couleur négatives"""
    image.show()
    l,h=image.size
    for ligne in range (0,h):
        for colonne in range (0,l):
            R,G,B= image.getpixel((colonne,ligne))
            nouveau_R = (255-R)
            nouveau_G = (255-G)
            nouveau_B = (255-B)
            image.putpixel((colonne,ligne),(nouveau_R,nouveau_G,nouveau_B))
    image.show()

def rotationcouleur():
    def rotationBRG():
        image = Image.open(nom_image)
        image.show()
        l,h=image.size
        for ligne in range (0,h):
             for colonne in range (0,l):
                 R,G,B = image.getpixel((colonne,ligne))
                 image.putpixel((colonne,ligne),(B,R,G))
        image.show()
        
    def rotationGBR ():
        image = Image.open(nom_image)
        image.show()
        l,h=image.size
        for ligne in range (0,h):
            for colonne in range (0,l):
                R,G,B = image.getpixel((colonne,ligne))
                image.putpixel((colonne,ligne),(G,B,R))
        image.show()
        
    def rotationBGR ():
        image = Image.open(nom_image)
        image.show()
        l,h=image.size
        for ligne in range (0,h):
            for colonne in range (0,l):
                R,G,B = image.getpixel((colonne,ligne))
                image.putpixel((colonne,ligne),(B,G,R))
        image.show()

    def affichermenu ():
        """affiche le menu"""
        print("1:rotation BRG")
        print("2:rotation GBR")
        print("3:rotation GBR")
        print("q:retourner au menu")
    
    choix="0"
    while choix!= "q":
        affichermenu()
        choix=input("choisir une action: ")
        if choix=="1":
            rotationBRG()
        elif choix=="2":
            rotationGBR()
        elif choix=="3":
            rotationBGR()
    
def symetrieImage():
    """ Faire la symétrie de l'image,elle peux être verticale ou horizontale """
    def symetrieVerticale():
        """ Faire la symétrie verticale de l'image """
        image = Image.open(nom_image)
        image.show()
        symetrie_verticale=image.transpose(Image.FLIP_TOP_BOTTOM)
        symetrie_verticale.show()

    def symetrieHorizontale(): 
        """Fait la symetrie horizontale de l'image""" 
        image = Image.open(nom_image)
        image.show()
        symetrie_horizontale=image.transpose(Image.FLIP_LEFT_RIGHT)
        symetrie_horizontale.show()

    def affichermenu ():
        print("1: symetrie verticale ")
        print("2: symetrie horizontale")
        print("q: retourner au menu")

    choix="0"
    while choix!= "q":
        affichermenu()
        choix=input("choisir une action: ")
        if choix=="1":
            symetrieVerticale()
        elif choix=="2":
            symetrieHorizontale()

def rotationImage():
    """ Faire une rotation de l'image, elle peux être à droite, à gauche ou peut _être retourner """
    def rotationdroite():
        """fait une rotation à droite""" 
        image = Image.open(nom_image)
        image.show()
        image.rotate(90).show()

    def rotationgauche():
        """fait une rotation à gauche""" 
        image = Image.open(nom_image)
        image.show()
        image.rotate(270).show()

    def rotationretourner():
        """fait une rotation à 180° de l'image"""
        image = Image.open(nom_image)
        image.show()
        image.rotate(180).show()

    def affichermenu ():
         """affiche le menu"""
         print("1:rotation à droite")
         print("2:rotatioin à gauche")
         print("3:demi-tour")
         print("q:retourner au menu")

    choix="0"
    while choix!= "q":
        affichermenu()
        choix=input("choisir une action: ")
        if choix=="1":
            rotationdroite()
        elif choix=="2":
            rotationgauche()
        elif choix=="3":
            rotationretourner()



print("1: Faire le négatif des couleurs")
print("2: Faire la rotation des couleurs")
print("3: Faire les symétrie de l'image")
print("4: Faire les rotation de l'image")
print("q: Quitter")

choix="0"
while choix!= "q":
    choix=input("choisir une action: ")
    if choix=="1":
        negatifcouleur()
    elif choix=="2":
        rotationcouleur()
    elif choix=="3":
        symetrieImage()
    elif choix=="4":
        rotationImage()
        